# ----------------------------------------------------------------------------
#
#   Color Reciter
#   Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

QT += core gui qml quick

CONFIG += c++14

QMAKE_CXXFLAGS += \
        -Wall \
        -Wno-empty-body \
        -Wno-register

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        color.cpp \
        color_database.cpp \
        display.cpp \
        main.cpp \
        preference.cpp \
        $$files(include/Little-CMS/src/*.c)

HEADERS += \
        color.h \
        color_database.h \
        display.h \
        main.h \
        preference.h \
        $$files(include/Little-CMS/include/*.h) \
        $$files(include/Little-CMS/src/*.h) \
        $$files(include/rapidjson/include/rapidjson/*.h)

INCLUDEPATH += \
        include/Little-CMS/include \
        include/rapidjson/include/rapidjson

RESOURCES += \
        qml.qrc \
        resource.qrc

macx: ICON = resource/color-reciter.icns
win32: RC_ICONS = resource/icon.ico

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
        ../.gitmodules \
        ../.gitignore \
        ../package/com.tumblr.nagianime.color.reciter/meta/installscript.qs \
        ../package/config-macx.xml \
        ../package/config-win32.xml \
        ../package/com.tumblr.nagianime.color.reciter/meta/package.xml

# Grab dlls on Windows
# cd build/release
# mkdir com.tumblr.nagianime.color.reciter && cd com.tumblr.nagianime.color.reciter
# cp ../color-reciter.exe .
# /c/Qt/5.15.1/mingw81_64/bin/windeployqt.exe -qmldir ../../../../src color-reciter.exe
# Deploy on Windows
# /c/Qt/Tools/QtInstallerFramework/3.2/bin/binarycreator.exe -p package -c package/config-win32.xml color-reciter-installer.exe
# /c/Qt/Tools/QtInstallerFramework/3.2/bin/repogen.exe -p package -i com.tumblr.nagianime.color.reciter repository
