import QtQuick 2.15

Text {
    text: ""

    anchors.top: parent.top
    anchors.horizontalCenter: parent.horizontalCenter

    font.family: font_extra_light.name
    font.pixelSize: 18
    color: Qt.rgba(color_data.display_text_color[0] / 255,
                   color_data.display_text_color[1] / 255,
                   color_data.display_text_color[2] / 255,
                   1)
}
