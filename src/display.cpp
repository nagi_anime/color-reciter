// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#include <QString>
#include <QStringList>

#include "preference.h"
#include "color.h"

#include "display.h"

ColorDisplay::ColorDisplay(ColorBase* parent): ColorBase(parent) {
    newColor();
}

bool ColorDisplay::isPreferenceValid() const {
    return !preference.isModifiedWithinLoading();
}

QString ColorDisplay::getPrimaryType() const {
    switch(preference.getPrimaryICCProfileType()) {
    case RGB: {
        return "RGB";
    }
    case HSV: {
        return "HSV";
    }
    case HSL: {
        return "HSL";
    }
    case Lab: {
        return "Lab";
    }
    case LCh: {
        return "LCh";
    }
    case first: case last: {
        return "";
    }
    }
    return "";
}
QString ColorDisplay::getSecondaryType() const {
    switch(preference.getSecondaryICCProfileType()) {
    case RGB: {
        return "RGB";
    }
    case HSV: {
        return "HSV";
    }
    case HSL: {
        return "HSL";
    }
    case Lab: {
        return "Lab";
    }
    case LCh: {
        return "LCh";
    }
    case first: case last: {
        return "";
    }
    }
    return "";
}
QString ColorDisplay::getTertiaryType() const {
    switch(preference.getTertiaryICCProfileType()) {
    case RGB: {
        return "RGB";
    }
    case HSV: {
        return "HSV";
    }
    case HSL: {
        return "HSL";
    }
    case Lab: {
        return "Lab";
    }
    case LCh: {
        return "LCh";
    }
    case first: case last: {
        return "";
    }
    }
    return "";
}
void ColorDisplay::setPrimaryType(QString primary_type) {
    if(primary_type == "RGB")
        preference.setPrimaryICCProfileType(RGB);
    else if(primary_type == "HSV")
        preference.setPrimaryICCProfileType(HSV);
    else if(primary_type == "HSL")
        preference.setPrimaryICCProfileType(HSL);
    else if(primary_type == "Lab")
        preference.setPrimaryICCProfileType(Lab);
    else if(primary_type == "LCh")
        preference.setPrimaryICCProfileType(LCh);

    emit onPrimaryTypeChanged();
}
void ColorDisplay::setSecondaryType(QString secondary_type) {
    if(secondary_type == "RGB")
        preference.setSecondaryICCProfileType(RGB);
    else if(secondary_type == "HSV")
        preference.setSecondaryICCProfileType(HSV);
    else if(secondary_type == "HSL")
        preference.setSecondaryICCProfileType(HSL);
    else if(secondary_type == "Lab")
        preference.setSecondaryICCProfileType(Lab);
    else if(secondary_type == "LCh")
        preference.setSecondaryICCProfileType(LCh);

    emit onSecondaryTypeChanged();
}
void ColorDisplay::setTertiaryType(QString tertiary_type) {
    if(tertiary_type == "RGB")
        preference.setTertiaryICCProfileType(RGB);
    else if(tertiary_type == "HSV")
        preference.setTertiaryICCProfileType(HSV);
    else if(tertiary_type == "HSL")
        preference.setTertiaryICCProfileType(HSL);
    else if(tertiary_type == "Lab")
        preference.setTertiaryICCProfileType(Lab);
    else if(tertiary_type == "LCh")
        preference.setTertiaryICCProfileType(LCh);

    emit onTertiaryTypeChanged();
}

QStringList ColorDisplay::getNameList() const {
    QStringList name_list;
    icc_profile_finder.getNameList(name_list);
    return name_list;
}
QString ColorDisplay::getPrimaryName() const {
    return icc_profile_finder.Path2Name(preference.getPrimaryICCProfilePath());
}
QString ColorDisplay::getSecondaryName() const {
    return icc_profile_finder.Path2Name(preference.getSecondaryICCProfilePath());
}
QString ColorDisplay::getTertiaryName() const {
    return icc_profile_finder.Path2Name(preference.getTertiaryICCProfilePath());
}
QString ColorDisplay::getDisplayName() const {
    return icc_profile_finder.Path2Name(preference.getDisplayICCProfilePath());
}
void ColorDisplay::setPrimaryName(QString primary_name) {
    preference.setPrimaryICCProfilePath(icc_profile_finder.Name2Path(primary_name));

    emit onPrimaryNameChanged();
}
void ColorDisplay::setSecondaryName(QString secondary_name) {
    preference.setSecondaryICCProfilePath(icc_profile_finder.Name2Path(secondary_name));

    emit onSecondaryNameChanged();
}
void ColorDisplay::setTertiaryName(QString tertiary_name) {
    preference.setTertiaryICCProfilePath(icc_profile_finder.Name2Path(tertiary_name));

    emit onTertiaryNameChanged();
}
void ColorDisplay::setDisplayName(QString display_name) {
    preference.setDisplayICCProfilePath(icc_profile_finder.Name2Path(display_name));

    emit onDisplayNameChanged();
}

Q_INVOKABLE void ColorDisplay::newColor() {
    color_converter.generate(m_primary_color,
                             m_secondary_color,
                             m_tertiary_color,
                             m_display_color,
                             m_display_text_color);

    emit onPrimaryColorChanged();
    emit onSecondaryColorChanged();
    emit onTertiaryColorChanged();
    emit onDisplayColorChanged();
    emit onDisplayTextColorChanged();
}
Q_INVOKABLE void ColorDisplay::setColor(QStringList color) {
    m_primary_color = color;
    color_converter.convert(m_primary_color,
                            m_secondary_color,
                            m_tertiary_color,
                            m_display_color,
                            m_display_text_color);

    emit onPrimaryColorChanged();
    emit onSecondaryColorChanged();
    emit onTertiaryColorChanged();
    emit onDisplayColorChanged();
    emit onDisplayTextColorChanged();
}
QStringList ColorDisplay::getPrimaryColor() const {
    return m_primary_color;
}
QStringList ColorDisplay::getSecondaryColor() const {
    return m_secondary_color;
}
QStringList ColorDisplay::getTertiaryColor() const {
    return m_tertiary_color;
}
QStringList ColorDisplay::getDisplayColor() const {
    return m_display_color;
}
QStringList ColorDisplay::getDisplayTextColor() const {
    return m_display_text_color;
}
