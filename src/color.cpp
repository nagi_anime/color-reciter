// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#include <QString>
#include <QStringList>
#include <QtMath>
#include <QRandomGenerator>

#include <cmath>

extern "C" {
#define CMS_NO_REGISTER_KEYWORD 1

#include "lcms2.h"
}

#include "preference.h"
#include "color.h"

// The color converter
ColorConverter color_converter;

// ColorBase -----------------------------------------------------------------

ColorBase::ColorBase(QObject* parent): QObject(parent) {}

// ColorConverter ------------------------------------------------------------

ColorConverter::ColorConverter(ColorBase* parent): ColorBase(parent) {}

void ColorConverter::convert(const QStringList& primary_color,
                             QStringList& secondary_color,
                             QStringList& tertiary_color,
                             QStringList& display_color,
                             QStringList& display_text_color) {
    convertFromPrimary(primary_color);

    convertTo(secondary_color,
              preference.isPrimaryICCProfileModified() || preference.isSecondaryICCProfileModified(),
              resetSecondaryModified,
              preference.getSecondaryICCProfileType(),
              preference.getSecondaryICCProfilePath(),
              fSecondary,
              hSecondary,
              hToSecondary);
    convertTo(tertiary_color,
              preference.isPrimaryICCProfileModified() || preference.isTertiaryICCProfileModified(),
              resetTertiaryModified,
              preference.getTertiaryICCProfileType(),
              preference.getTertiaryICCProfilePath(),
              fTertiary,
              hTertiary,
              hToTertiary);
    convertTo(display_color,
              preference.isPrimaryICCProfileModified() || preference.isDisplayICCProfileModified(),
              resetDisplayModified,
              RGB,
              preference.getDisplayICCProfilePath(),
              fDisplay,
              hDisplay,
              hToDisplay);
    convertToDisplayTextColor(display_text_color);

    if(preference.isPrimaryICCProfileModified())
        preference.resetPrimaryICCProfileModified();
}
void ColorConverter::generate(QStringList& primary_color,
                              QStringList& secondary_color,
                              QStringList& tertiary_color,
                              QStringList& display_color,
                              QStringList& display_text_color) {
    switch(preference.getPrimaryICCProfileType()) {
    case RGB: {
        for(int i: {0, 1, 2})
            primary_color[i] = QString::number(QRandomGenerator::global()->bounded(256));
    } break;

    case HSV:
    case HSL: {
        for(int i: {0})
            primary_color[i] = QString::number(QRandomGenerator::global()->bounded(360));
        for(int i: {1, 2})
            primary_color[i] = QString::number(QRandomGenerator::global()->bounded(101));
    } break;

    case Lab: {
        for(int i: {0})
            primary_color[i] = QString::number(QRandomGenerator::global()->bounded(101));
        for(int i: {1, 2})
            primary_color[i] = QString::number(QRandomGenerator::global()->bounded(151));
    } break;
    case LCh: {
        for(int i: {0}) primary_color[i] =
                QString::number(QRandomGenerator::global()->bounded(101));
        for(int i: {1}) primary_color[i] =
                QString::number(QRandomGenerator::global()->bounded(213));
        for(int i: {2}) primary_color[i] =
                QString::number(QRandomGenerator::global()->bounded(360));
    } break;

    case first: case last: break;
    }

    convert(primary_color, secondary_color, tertiary_color, display_color, display_text_color);
}

void ColorConverter::convertFromsRGB(const QStringList& sRGB_color,
                                     QStringList& primary_color) {
    if(preference.isPrimaryICCProfileModified()) {
        switch(preference.getPrimaryICCProfileType()) {
        case RGB:
        case HSV:
        case HSL: {
            fPrimary = TYPE_RGB_DBL;
            hPrimary = cmsOpenProfileFromFile(preference.getPrimaryICCProfilePath().toStdString().c_str(), "r");
        } break;
        case Lab:
        case LCh: {
            fPrimary = TYPE_Lab_DBL;
            hPrimary = cmsCreateLab4Profile(nullptr);
        } break;

        case first: case last: break;
        }

        // reset will be performed at convert()
    }

    if(fsRGB != TYPE_RGB_DBL) {
        fsRGB = TYPE_RGB_DBL;
        hsRGB = cmsCreate_sRGBProfile();

        hFromsRGB = cmsCreateTransform(hsRGB, fsRGB, hPrimary, fPrimary, INTENT_PERCEPTUAL, 0);
    }

    cmsFloat64Number csRGB[3] = {
        sRGB_color[0].toDouble() / 255,
        sRGB_color[1].toDouble() / 255,
        sRGB_color[2].toDouble() / 255
    };

    cmsDoTransform(hFromsRGB, csRGB, &cBuffer, 1);

    convertTo1(cBuffer, primary_color, preference.getPrimaryICCProfileType());
}

void ColorConverter::convertFromPrimary(const QStringList& primary_color) {
    switch(preference.getPrimaryICCProfileType()) {
    case RGB: {
        for(int i: {0, 1, 2}) cPrimary[i] = primary_color[i].toDouble() / 255;
    } break;

    case HSV: {
        auto f = [primary_color](int n) -> double {
            double k = std::fmod((n + primary_color[0].toDouble() / 60), 6);
            double S = primary_color[1].toDouble() / 100;
            double V = primary_color[2].toDouble() / 100;
            return V - V * S * qMax(0.0, qMin(1.0, qMin(k, 4 - k)));
        };

        cPrimary[0] = f(5);
        cPrimary[1] = f(3);
        cPrimary[2] = f(1);
    } break;

    case HSL: {
        auto f = [primary_color](int n) -> double {
            double k = std::fmod((n + primary_color[0].toDouble() / 30), 12);
            double S = primary_color[1].toDouble() / 100;
            double L = primary_color[2].toDouble() / 100;
            return L - S * qMin(L, 1 - L) * qMax(-1.0, qMin(1.0, qMin(k - 3, 9 - k)));
        };

        cPrimary[0] = f(0);
        cPrimary[1] = f(8);
        cPrimary[2] = f(4);
    } break;

    case Lab: {
        for(int i: {0, 1, 2}) cPrimary[i] = primary_color[i].toDouble() / 100;
    } break;

    case LCh: {
        double C = primary_color[1].toDouble();
        double h = primary_color[2].toDouble();

        cPrimary[0] = primary_color[0].toDouble();
        cPrimary[1] = C * qCos(h);
        cPrimary[2] = C * qSin(h);
    } break;

    case first: case last: break;
    }

    if(preference.isPrimaryICCProfileModified()) {
        switch(preference.getPrimaryICCProfileType()) {
        case RGB:
        case HSV:
        case HSL: {
            fPrimary = TYPE_RGB_DBL;
            hPrimary = cmsOpenProfileFromFile(preference.getPrimaryICCProfilePath().toStdString().c_str(), "r");
        } break;
        case Lab:
        case LCh: {
            fPrimary = TYPE_Lab_DBL;
            hPrimary = cmsCreateLab4Profile(nullptr);
        } break;

        case first: case last: break;
        }

        // will reset at the end of convert()
        // preference.resetPrimaryICCProfileModified();
    }
}

void ColorConverter::convertTo(QStringList& color,
                               bool is_modified,
                               void (& reset_modified)(),
                               ICCProfileType type,
                               ICCProfilePath path,
                               cmsUInt32Number fType,
                               cmsHPROFILE& hProfile,
                               cmsHTRANSFORM& hTransform) {
    if(is_modified) {
        switch(type) {
        case RGB:
        case HSV:
        case HSL: {
            fType = TYPE_RGB_DBL;
            hProfile = cmsOpenProfileFromFile(path.toStdString().c_str(), "r");
        } break;
        case Lab:
        case LCh: {
            fType = TYPE_Lab_DBL;
            hProfile = cmsCreateLab4Profile(nullptr);
        } break;

        case first: case last: break;
        }

        hTransform = cmsCreateTransform(hPrimary, fPrimary, hProfile, fType, INTENT_PERCEPTUAL, 0);

        reset_modified();
    }

    cmsDoTransform(hTransform, cPrimary, &cBuffer, 1);

    convertTo1(cBuffer, color, type);
}
void ColorConverter::resetPrimaryModified() {
    preference.resetPrimaryICCProfileModified();
}
void ColorConverter::resetSecondaryModified() {
    preference.resetSecondaryICCProfileModified();
}
void ColorConverter::resetTertiaryModified() {
    preference.resetTertiaryICCProfileModified();
}
void ColorConverter::resetDisplayModified() {
    preference.resetDisplayICCProfileModified();
}

void ColorConverter::convertTo1(double cColor[3], QStringList& color, ICCProfileType type) {
    switch(type) {
    case RGB: {
        for(int i: {0, 1, 2}) color[i] = QString::number(qRound(cColor[i] * 255));
    } break;

    case HSV: {
        double R = cColor[0];
        double G = cColor[1];
        double B = cColor[2];

        double V = qMax(qMax(R, G), B);
        double C = V - qMin(qMin(R, G), B);
        double H;
        if(fabs(C - 0) < color_epsilon) H = 0;
        else if(fabs(V - R) < color_epsilon) H = 60 * (0 + (G - B) / C);
        else if(fabs(V - G) < color_epsilon) H = 60 * (0 + (B - R) / C);
        else if(fabs(V - B) < color_epsilon) H = 60 * (0 + (R - G) / C);
        else H = 0; // never reach
        double S;
        if(fabs(V - 0) < color_epsilon) S = 0;
        else S = C / V;

        color[0] = QString::number(qRound(H));
        color[1] = QString::number(qRound(S * 100));
        color[2] = QString::number(qRound(V * 100));
    }  break;

    case HSL: {
        double R = cColor[0];
        double G = cColor[1];
        double B = cColor[2];

        double V = qMax(qMax(R, G), B);
        double C = V - qMin(qMin(R, G), B);
        double H;
        if(fabs(C - 0) < color_epsilon) H = 0;
        else if(fabs(V - R) < color_epsilon) H = 60 * (0 + (G - B) / C);
        else if(fabs(V - G) < color_epsilon) H = 60 * (0 + (B - R) / C);
        else if(fabs(V - B) < color_epsilon) H = 60 * (0 + (R - G) / C);
        else H = 0; // never reach
        double S;
        if(fabs(V - 0) < color_epsilon) S = 0;
        else S = C / V;

        color[0] = QString::number(qRound(H));
        color[1] = QString::number(qRound(S * 100));
        color[2] = QString::number(qRound((V - C / 2) * 100));
    } break;

    case Lab: {
        for(int i: {0, 1, 2}) color[i] = QString::number(qRound(cColor[i]));
    } break;

    case LCh: {
        color[0] = QString::number(qRound(cColor[0]));
        color[1] = QString::number(qRound(qSqrt(qPow(cColor[1], 2) + qPow(cColor[2], 2))));
        color[2] = QString::number(qRound(qAtan2(cColor[2], cColor[1])));
    } break;

    case first: case last: break;
    }
}

void ColorConverter::convertToDisplayTextColor(QStringList& display_text_color) {
    if(preference.isPrimaryICCProfileModified()) {
        fDisplayText = TYPE_XYZ_DBL;
        hDisplayText = cmsCreateXYZProfile();

        hToDisplayText = cmsCreateTransform(hPrimary, fPrimary, hDisplayText, fDisplayText, INTENT_PERCEPTUAL, 0);
    }

    cmsDoTransform(hToDisplayText, cPrimary, &cBuffer, 1);

    if(cBuffer[1] > 0.5) // Y in CIEXYZ is the relative luminance in WCAG 2.0
        for(int i: {0, 1, 2}) display_text_color[i] = QString::number(0);
    else for(int i: {0, 1, 2}) display_text_color[i] = QString::number(255);
}
