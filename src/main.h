// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#ifndef MAIN_H
#define MAIN_H

#include <QObject>
#include <QStringList>

int main(int argc, char *argv[]);

class ColorClipboard: public QObject {
    Q_OBJECT
public:
    explicit ColorClipboard(QObject* parent = nullptr);

    Q_INVOKABLE QStringList getColor() const;
    Q_INVOKABLE void setColor(const QStringList& color);
};

#endif // MAIN_H
