import QtQuick 2.15

Text {
    text: ", "

    font.family: font_extra_light.name
    font.pixelSize: 96
    color: Qt.rgba(color_data.display_text_color[0] / 255,
                   color_data.display_text_color[1] / 255,
                   color_data.display_text_color[2] / 255,
                   1)
}
