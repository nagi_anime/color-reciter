import QtQuick 2.15

Text {
    text: ""

    visible: false
    anchors.horizontalCenter: parent.horizontalCenter
    horizontalAlignment: Text.AlignHCenter

    font.family: font_light.name
    font.pixelSize: 24
    color: Qt.rgba(color_data.display_text_color[0] / 255,
                   color_data.display_text_color[1] / 255,
                   color_data.display_text_color[2] / 255,
                   1)
}
