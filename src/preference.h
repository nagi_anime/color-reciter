// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#ifndef PREFERENCE_H
#define PREFERENCE_H

#include <QObject>
#include <QDir>
#include <QFileInfo>
#include <QStandardPaths>
#include <QString>
#include <QMap>

#include "document.h"

#include "color.h"

// Preference ----------------------------------------------------------------

class Preference: public ColorBase {
    Q_OBJECT
public:
    explicit Preference (ColorBase* parent = nullptr);

    ICCProfileType getPrimaryICCProfileType();
    void setPrimaryICCProfileType(const ICCProfileType type);
    ICCProfilePath getPrimaryICCProfilePath();
    void setPrimaryICCProfilePath(const ICCProfilePath& path);
    ICCProfileType getSecondaryICCProfileType();
    void setSecondaryICCProfileType(const ICCProfileType type);
    ICCProfilePath getSecondaryICCProfilePath();
    void setSecondaryICCProfilePath(const ICCProfilePath& path);
    ICCProfileType getTertiaryICCProfileType();
    void setTertiaryICCProfileType(const ICCProfileType type);
    ICCProfilePath getTertiaryICCProfilePath();
    void setTertiaryICCProfilePath(const ICCProfilePath& path);
    ICCProfilePath getDisplayICCProfilePath();
    void setDisplayICCProfilePath(const ICCProfilePath& path);

    bool isPrimaryICCProfileModified() const;
    void resetPrimaryICCProfileModified();
    bool isSecondaryICCProfileModified() const;
    void resetSecondaryICCProfileModified();
    bool isTertiaryICCProfileModified() const;
    void resetTertiaryICCProfileModified();
    bool isDisplayICCProfileModified() const;
    void resetDisplayICCProfileModified();
private:
    void loadPreference();

    void checkBuiltinICCProfiles();
    void validatePreference();
    void savePreference();
public:
    bool isModifiedWithinLoading();
private:
    bool is_modified_within_loading = false;

    bool is_loaded = false;

    // The preference in the RAM
    rapidjson::Document preference;

public:
    // Default preference file location
    const QDir preference_directory =
            QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) +
            QDir::separator() + "color-reciter";
    const QFileInfo preference_file = QFileInfo(preference_directory, "preference.json");

    const QDir builtin_icc_profiles_directory =
            preference_directory.absolutePath() +
            QDir::separator() + "icc_profiles";

private:
    // buffer with a standard size of 1024
    char* buffer = nullptr;
public:
    void freeBuffer();

    bool primary_icc_profile_modified = true;
    bool secondary_icc_profile_modified = true;
    bool tertiary_icc_profile_modified = true;
    bool display_icc_profile_modified = true;
};

// ColorICCProfileFinder -----------------------------------------------------

class ColorICCProfileFinder: public ColorBase {
    Q_OBJECT
public:
    explicit ColorICCProfileFinder(ColorBase* parent = nullptr);

private:
    void loadNameList();
    void loadNameList1(QStringList& locations, QStringList& filter, QString comment = QString());
public:
    void getNameList(QStringList& name_list);
    QString Name2Path(const QString& name);
    QString Path2Name(const QString& path);

private:
    QMap<QString, QString> name_map;
};

extern Preference preference;

extern ColorICCProfileFinder icc_profile_finder;

#endif // PREFERENCE_H
