// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QClipboard>
#include <QString>
#include <QStringList>
#include <QColor>

#include "display.h"

#include "main.h"

int main(int argc, char *argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    ColorDisplay color_data;
    engine.rootContext()->setContextProperty("color_data", &color_data);
    ColorClipboard color_clipboard;
    engine.rootContext()->setContextProperty("color_clipboard", &color_clipboard);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if(!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

ColorClipboard::ColorClipboard(QObject* parent): QObject(parent) {}
Q_INVOKABLE QStringList ColorClipboard::getColor() const {
    QStringList pasted_color = {"-1", "-1", "-1"};
    QString arg = QGuiApplication::clipboard()->text();

    if(!arg.isEmpty() && arg.front() == QChar('#')) {
        QColor color;
        color.setNamedColor(arg);

        if(color.isValid()) {
            QStringList sRGB_color = {
                QString::number(color.red()),
                QString::number(color.green()),
                QString::number(color.blue())};

            color_converter.convertFromsRGB(sRGB_color, pasted_color);

            return pasted_color;
        }
    }
    else if(!arg.isEmpty() && (arg.front() == QChar('(')) && (arg.back() == QChar(')'))) {
        arg.remove(0, 1); arg.remove(arg.length() - 1, 1);
        QStringList colors = arg.split(",");

        for(auto it = colors.begin(); it != colors.end(); ++it)
            *it = it->trimmed();

        bool ok;
        bool is_valid = true;
        for(int i: {0, 1, 2}) {
            colors[i].toInt(&ok);
            if(!ok) is_valid = false;
        }

        if(is_valid) {
            for(int i: {0, 1, 2})
                pasted_color[i] = QString::number(colors[i].toInt());

            return pasted_color;
        }
    }
    return pasted_color;
}
Q_INVOKABLE void ColorClipboard::setColor(const QStringList& color) {
    QString colors = "(" + color[0] + ", " + color[1] + ", " + color[2] + ")";
    QGuiApplication::clipboard()->setText(colors);
}
