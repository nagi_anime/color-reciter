// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#ifndef DISPLAY_H
#define DISPLAY_H

#include <QObject>
#include <QString>
#include <QStringList>

#include "preference.h"

class ColorDisplay: public ColorBase {
    Q_OBJECT
public:
    explicit ColorDisplay(ColorBase* parent = nullptr);

    Q_PROPERTY(bool is_preference_valid READ isPreferenceValid CONSTANT)
    Q_PROPERTY(QStringList name_list READ getNameList CONSTANT)
    Q_PROPERTY(QString primary_type READ getPrimaryType WRITE setPrimaryType NOTIFY onPrimaryTypeChanged)
    Q_PROPERTY(QString primary_name READ getPrimaryName WRITE setPrimaryName NOTIFY onPrimaryNameChanged)
    Q_PROPERTY(QString secondary_type READ getSecondaryType WRITE setSecondaryType NOTIFY onSecondaryTypeChanged)
    Q_PROPERTY(QString secondary_name READ getSecondaryName WRITE setSecondaryName NOTIFY onSecondaryNameChanged)
    Q_PROPERTY(QString tertiary_type READ getTertiaryType WRITE setTertiaryType NOTIFY onTertiaryTypeChanged)
    Q_PROPERTY(QString tertiary_name READ getTertiaryName WRITE setTertiaryName NOTIFY onTertiaryNameChanged)
    Q_PROPERTY(QString display_name READ getDisplayName WRITE setDisplayName NOTIFY onDisplayNameChanged)
public:
    bool isPreferenceValid() const;
    QStringList getNameList() const;
    QString getPrimaryType() const;
    void setPrimaryType(QString primary_type);
    QString getPrimaryName() const;
    void setPrimaryName(QString primary_name);
    QString getSecondaryType() const;
    void setSecondaryType(QString secondary_type);
    QString getSecondaryName() const;
    void setSecondaryName(QString secondary_name);
    QString getTertiaryType() const;
    void setTertiaryType(QString tertiary_type);
    QString getTertiaryName() const;
    void setTertiaryName(QString tertiary_name);
    QString getDisplayName() const;
    void setDisplayName(QString display_name);
signals:
    void onPrimaryTypeChanged();
    void onPrimaryNameChanged();
    void onSecondaryTypeChanged();
    void onSecondaryNameChanged();
    void onTertiaryTypeChanged();
    void onTertiaryNameChanged();
    void onDisplayNameChanged();
public:
    Q_INVOKABLE void newColor();
    Q_INVOKABLE void setColor(QStringList color);

    Q_PROPERTY(QStringList primary_color READ getPrimaryColor NOTIFY onPrimaryColorChanged)
    Q_PROPERTY(QStringList secondary_color READ getSecondaryColor NOTIFY onSecondaryColorChanged)
    Q_PROPERTY(QStringList tertiary_color READ getTertiaryColor NOTIFY onTertiaryColorChanged)
    Q_PROPERTY(QStringList display_color READ getDisplayColor NOTIFY onDisplayColorChanged)
    Q_PROPERTY(QStringList display_text_color READ getDisplayTextColor NOTIFY onDisplayTextColorChanged)
public:
    QStringList getPrimaryColor() const;
    QStringList getSecondaryColor() const;
    QStringList getTertiaryColor() const;
    QStringList getDisplayColor() const;
    QStringList getDisplayTextColor() const;
signals:
    void onPrimaryColorChanged();
    void onSecondaryColorChanged();
    void onTertiaryColorChanged();
    void onDisplayColorChanged();
    void onDisplayTextColorChanged();
private:
    QStringList m_primary_color = {"0", "0", "0"};
    QStringList m_secondary_color = {"0", "0", "0"};
    QStringList m_tertiary_color = {"0", "0", "0"};
    QStringList m_display_color = {"0", "0", "0"};
    QStringList m_display_text_color = {"0", "0", "0"};
};

#endif // DISPLAY_H
