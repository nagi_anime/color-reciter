// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

ApplicationWindow {
    id: root
    width: 1280
    height: 720
    minimumWidth: 1024
    minimumHeight: 576
    visible: true
    title: qsTr("Color Reciter")

    Material.theme: Material.System
    Material.accent: Qt.rgba(color_data.display_color[0] / 255,
                             color_data.display_color[1] / 255,
                             color_data.display_color[2] / 255,
                             1)
    // background color
    Rectangle {
        anchors.fill: parent

        color: Qt.rgba(color_data.display_color[0] / 255,
                       color_data.display_color[1] / 255,
                       color_data.display_color[2] / 255,
                       1)
    }
    FontLoader {
        id: font_extra_light
        source: "qrc:///fonts/SourceHanSans-ExtraLight.otf"
    }
    FontLoader {
        id: font_light
        source: "qrc:///fonts/SourceHanSans-Light.otf"
    }

    // onLoadedReloaded ------------------------------------------------------
    Component.onCompleted: onLoadedReloaded()
    function onLoadedReloaded() {
        updateUpper()
    }
    function updateUpper() {
        switch(color_data.primary_type) {
        case "RGB":
            upper_first_indicator.text = "R"
            upper_second_indicator.text = "G"
            upper_third_indicator.text = "B"

            upper_first_number.max_text = 255
            upper_second_number.max_text = 255
            upper_third_number.max_text = 255

            break
        case "HSV":
            upper_first_indicator.text = "H"
            upper_second_indicator.text = "S"
            upper_third_indicator.text = "V"

            upper_first_number.max_text = 360
            upper_second_number.max_text = 100
            upper_third_number.max_text = 100

            break
        case "HSL":
            upper_first_indicator.text = "H"
            upper_second_indicator.text = "S"
            upper_third_indicator.text = "L"

            upper_first_number.max_text = 360
            upper_second_number.max_text = 100
            upper_third_number.max_text = 100

            break
        case "Lab":
            upper_first_indicator.text = "L"
            upper_second_indicator.text = "a"
            upper_third_indicator.text = "b"

            upper_first_number.max_text = 100
            upper_second_number.max_text = 999
            upper_third_number.max_text = 999

            break
        case "LCh":
            upper_first_indicator.text = "L"
            upper_second_indicator.text = "C"
            upper_third_indicator.text = "h"

            upper_first_number.max_text = 100
            upper_second_number.max_text = 999
            upper_third_number.max_text = 999

            break
        }

        onNewColor()
    }

    // onNewColor ------------------------------------------------------------
    function onNewColor() {
        color_data.newColor()

        updateColor()
        updateLowerBlock()
    }

    function updateColor() {
        upper_first_number.text = ""
        upper_second_number.text = ""
        upper_third_number.text = ""
        user_color[0] = ""
        user_color[1] = ""
        user_color[2] = ""

        upper_first_number.focus = true
    }
    function updateLowerBlock() {
        lower_second_text.visible = false
        lower_third_text.visible = false

        switch(color_data.secondary_type) {
        case "RGB":
            lower_second_text.text =
                    "R " + color_data.secondary_color[0] +
                    ", G " + color_data.secondary_color[1] +
                    ", B " + color_data.secondary_color[2]
            if(color_data.secondary_name != "")
                lower_second_text.text += " (" + color_data.secondary_name + ")"
            break
        case "HSV":
            lower_second_text.text =
                    "H " + color_data.secondary_color[0] +
                    ", S " + color_data.secondary_color[1] +
                    ", V " + color_data.secondary_color[2]
            if(color_data.secondary_name != "")
                lower_second_text.text += " (" + color_data.secondary_name + ")"
            break
        case "HSL":
            lower_second_text.text =
                    "H " + color_data.secondary_color[0] +
                    ", S " + color_data.secondary_color[1] +
                    ", L " + color_data.secondary_color[2]
            if(color_data.secondary_name != "")
                lower_second_text.text += " (" + color_data.secondary_name + ")"
            break
        case "Lab":
            lower_second_text.text =
                    "L* " + color_data.secondary_color[0] +
                    ", a* " + color_data.secondary_color[1] +
                    ", b* " + color_data.secondary_color[2]
            if(color_data.secondary_name != "")
                lower_second_text.text += " (" + color_data.secondary_name + ")"
            break
        case "LCh":
            lower_second_text.text =
                    "L* " + color_data.secondary_color[0] +
                    ", C* " + color_data.secondary_color[1] +
                    ", h " + color_data.secondary_color[2]
            if(color_data.secondary_name != "")
                lower_second_text.text += " (" + color_data.secondary_name + ")"
            break
        case 0:
        case 6:
            break
        }

        switch(color_data.tertiary_type) {
        case "RGB":
            lower_third_text.text =
                    "R " + color_data.tertiary_color[0] +
                    ", G " + color_data.tertiary_color[1] +
                    ", B " + color_data.tertiary_color[2]
            if(color_data.tertiary_name != "")
                lower_third_text.text += " (" + color_data.tertiary_name + ")"
            break
        case "HSV":
            lower_third_text.text =
                    "H " + color_data.tertiary_color[0] +
                    ", S " + color_data.tertiary_color[1] +
                    ", V " + color_data.tertiary_color[2]
            if(color_data.tertiary_name != "")
                lower_third_text.text += " (" + color_data.tertiary_name + ")"
            break
        case "HSL":
            lower_third_text.text =
                    "H " + color_data.tertiary_color[0] +
                    ", S " + color_data.tertiary_color[1] +
                    ", L " + color_data.tertiary_color[2]
            if(color_data.tertiary_name != "")
                lower_third_text.text += " (" + color_data.tertiary_name + ")"
            break
        case "Lab":
            lower_third_text.text =
                    "L* " + color_data.tertiary_color[0] +
                    ", a* " + color_data.tertiary_color[1] +
                    ", b* " + color_data.tertiary_color[2]
            if(color_data.tertiary_name != "")
                lower_third_text.text += " (" + color_data.tertiary_name + ")"
            break
        case "LCh":
            lower_third_text.text =
                    "L* " + color_data.tertiary_color[0] +
                    ", C* " + color_data.tertiary_color[1] +
                    ", h " + color_data.tertiary_color[2]
            if(color_data.tertiary_name != "")
                lower_third_text.text += " (" + color_data.tertiary_name + ")"
            break
        case 0:
        case 6:
            break
        }
    }

    // onCopyColor & onPasteColor --------------------------------------------
    function onCopyColor() {
        color_clipboard.setColor(color_data.primary_color)
    }
    function onPasteColor() {
        var pasted_color = color_clipboard.getColor()
        if(pasted_color[0] !== "-1")
            color_data.setColor(pasted_color)

        updateColor()
        updateLowerBlock()
    }

    // onRequestCorrectColor and onRequestUserColor --------------------------
    property var user_color: ["0", "0", "0"]
    property bool is_requested: false
    function onRequestCorrectColor() {
        if(!is_requested) {
            upper_first_number.readOnly = true
            upper_second_number.readOnly = true
            upper_third_number.readOnly = true

            user_color[0] = upper_first_number.text
            user_color[1] = upper_second_number.text
            user_color[2] = upper_third_number.text

            upper_first_number.text = color_data.primary_color[0]
            upper_second_number.text = color_data.primary_color[1]
            upper_third_number.text = color_data.primary_color[2]

            lower_second_text.visible = true
            lower_third_text.visible = true

            is_requested = true
        }
    }
    function onRequestUserColor() {
        if(is_requested) {
            upper_first_number.readOnly = false
            upper_second_number.readOnly = false
            upper_third_number.readOnly = false

            upper_first_number.text = user_color[0]
            upper_second_number.text = user_color[1]
            upper_third_number.text = user_color[2]

            lower_second_text.visible = false
            lower_third_text.visible = false

            is_requested = false
        }
    }

    Column {
        anchors.fill: parent

        Item {
            width: parent.width
            height: parent.height * 0.55

            Row {
                id: upper_block

                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Item {
                    width: 160
                    height: 139

                    UpperIndicator {
                        id: upper_first_indicator
                    }
                    UpperNumber {
                        id: upper_first_number

                        KeyNavigation.tab: upper_second_number
                    }
                }

                UpperComma {
                }

                Item {
                    width: 160
                    height: 139

                    UpperIndicator {
                        id: upper_second_indicator
                    }
                    UpperNumber {
                        id: upper_second_number

                        KeyNavigation.tab: upper_third_number
                    }
                }

                UpperComma {
                }

                Item {
                    width: 160
                    height: 139

                    UpperIndicator {
                        id: upper_third_indicator
                    }
                    UpperNumber {
                        id: upper_third_number

                        KeyNavigation.tab: upper_first_number
                    }
                }
            }
        }

        Item {
            width: parent.width
            height: parent.height * 0.45

            Column {
                id: lower_block

                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter

                spacing: 8

                LowerText {
                    id: lower_third_text
                }
                LowerText {
                    id: lower_second_text
                }
            }
        }
    }

    PreferencePane {
        id: preference
    }
}
