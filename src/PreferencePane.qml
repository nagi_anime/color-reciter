import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

Pane {
    width: parent.width
    height: parent.height - 96
    y: parent.height

    Material.elevation: 16

    // onTypeChanged & onNameChanged -----------------------------------------
    function onPrimaryTypeChanged(primary_type) {
        color_data.primary_type = primary_type
    }
    function onPrimaryNameChanged(primary_name) {
        color_data.primary_name = primary_name
    }
    function onSecondaryTypeChanged(secondary_type) {
        color_data.secondary_type = secondary_type
    }
    function onSecondaryNameChanged(secondary_name) {
        color_data.secondary_name = secondary_name
    }
    function onTertiaryTypeChanged(tertiary_type) {
        color_data.tertiary_type = tertiary_type
    }
    function onTertiaryNameChanged(tertiary_name) {
        color_data.tertiary_name = tertiary_name
    }
    function onDisplayNameChanged(display_name) {
        color_data.display_name = display_name
    }

    // is_modified -----------------------------------------------------------
    property bool is_modified

    // onOpen ----------------------------------------------------------------
    signal open()
    property bool is_opened: false
    onOpen: {
//        SequentialAnimation
        if(is_opened) {
            preference_close.start()
            if(is_modified) updateUpper()
            upper_first_number.focus = true
        }
        else {
            preference_open.start()

            is_modified = false
        }
        is_opened = !is_opened
    }
    Shortcut {
        sequences: ["Ctrl+,"]
        onActivated: {
            preference.open()
        }
    }
    Component.onCompleted: {
        if(!color_data.is_preference_valid)
            preference.open()
    }

    NumberAnimation {
        id: preference_open
        target: preference
        property: "y"
        from: parent.height
        to: 96
        duration: 300
        easing.type: Easing.OutExpo
    }
    NumberAnimation {
        id: preference_close
        target: preference
        property: "y"
        from: 96
        to: parent.height
        duration: 300
        easing.type: Easing.OutExpo
    }

    Item {
        width: (parent.height / 3 * 4) - 18 * 2
        height: parent.height - 54 * 2
        anchors.centerIn: parent

        ColumnLayout {
            anchors.fill: parent
            spacing: 18

            GridLayout {
                Layout.fillWidth: true

                columns: 3
                columnSpacing: 12
                rowSpacing: 5

                Text {
                    text: "Primary Color:"
                    Layout.fillHeight: true

                    verticalAlignment: Text.AlignVCenter
                    font.family: font_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.83
                }
                ComboBox {
                    id: preference_primary_type

                    model: [ "RGB", "HSV", "HSL", "Lab", "LCh" ]
                    Component.onCompleted: {
                        currentIndex = find(color_data.primary_type)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onPrimaryTypeChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                ComboBox {
                    id: preference_primary_name
                    Layout.fillWidth: true

                    model: color_data.name_list
                    Component.onCompleted: {
                        currentIndex = find(color_data.primary_name)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onPrimaryNameChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                Text {
                    text: "Tertiary Color:"
                    Layout.fillHeight: true

                    verticalAlignment: Text.AlignVCenter
                    font.family: font_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.83
                }
                ComboBox {
                    id: preference_tertiary_type

                    model: [ "RGB", "HSV", "HSL", "Lab", "LCh" ]
                    Component.onCompleted: {
                        currentIndex = find(color_data.tertiary_type)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onTertiaryTypeChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                ComboBox {
                    id: preference_tertiary_name
                    Layout.fillWidth: true

                    model: color_data.name_list
                    Component.onCompleted: {
                        currentIndex = find(color_data.tertiary_name)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onTertiaryNameChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                Text {
                    text: "Secondary Color:"
                    Layout.fillHeight: true

                    verticalAlignment: Text.AlignVCenter
                    font.family: font_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.83
                }
                ComboBox {
                    id: preference_secondary_type

                    model: [ "RGB", "HSV", "HSL", "Lab", "LCh" ]
                    Component.onCompleted: {
                        currentIndex = find(color_data.secondary_type)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onSecondaryTypeChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                ComboBox {
                    id: preference_secondary_name
                    Layout.fillWidth: true

                    model: color_data.name_list
                    Component.onCompleted: {
                        currentIndex = find(color_data.secondary_name)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onSecondaryNameChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
                Text {
                    text: "Display Color:"
                    Layout.topMargin: 13
                    Layout.fillHeight: true

                    verticalAlignment: Text.AlignVCenter
                    font.family: font_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.83
                }
                ComboBox {
                    id: preference_display_name
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    Layout.topMargin: 13

                    model: color_data.name_list
                    Component.onCompleted: {
                        currentIndex = find(color_data.display_name)
                    }
                    property bool is_first_run: true
                    onCurrentIndexChanged: {
                        if(!is_first_run) onDisplayNameChanged(textAt(currentIndex))
                        else is_first_run = false

                        is_modified = true
                    }

                    font.family: font_light.name
                }
            }

            ScrollView {
                clip: true
                Layout.fillWidth: true

                Text {
                    font.family: font_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.83

                    text: "\
Press <Return> to start; Type in what your guess; Press <Tab> to Switch between three parameters;
Press <Space> to preview the result; Press <Control-C> to copy the result in primary color.
Press <Return> to pick up a new random color; Press <Control-V> to paste a color.
And if you meet any problems, press <Control-,> to open up this preference page again.\
"
                }
            }

            ScrollView {
                clip: true
                Layout.fillWidth: true
                Layout.fillHeight: true

                Text {
                    font.family: font_extra_light.name
                    font.pixelSize: 16

                    color: Material.foreground
                    opacity: 0.60

                    text: "\
Color Reciter
Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.


using library:
Little CMS
Copyright (c) 1998-2020 Marti Maria Saguer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


using library:
RapidJSON
Copyright (C) 2015 THL A29 Limited, a Tencent company, and Milo Yip. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


using library:
The msinttypes r29
Copyright (c) 2006-2013 Alexander Chemeris
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
* Neither the name of  copyright holder nor the names of its contributors may
be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


using library:
json.org
Copyright (c) 2002 JSON.org
All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


using library:
JSON_checker
Copyright (c) 2002 JSON.org
All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.\
"
                }
            }
        }
    }
}
