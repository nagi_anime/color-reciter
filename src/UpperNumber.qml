import QtQuick 2.15

TextInput {
    text: ""
    validator: IntValidator {
        bottom: 0
        top: 255
    }

    anchors.fill: parent
    horizontalAlignment: TextInput.AlignRight

    Keys.onPressed: {
        if(event.key === Qt.Key_Space) {
            onRequestCorrectColor()

            event.accept = true
        }
        else if((event.key === Qt.Key_C) && (event.modifiers === Qt.ControlModifier)) {
            onCopyColor()

            event.accept = true
        }
        else if((event.key === Qt.Key_V) && (event.modifiers === Qt.ControlModifier)) {
            onPasteColor()

            event.accept = true
        }
        else if(((event.key === Qt.Key_A) && (event.modifiers === Qt.ControlModifier)) ||
                ((event.key === Qt.Key_Left) && (event.modifiers === Qt.ShiftModifier)) ||
                ((event.key === Qt.Key_Right) && (event.modifiers === Qt.ShiftModifier)) ||
                ((event.key === Qt.Key_Up) && (event.modifiers === Qt.ShiftModifier)) ||
                ((event.key === Qt.Key_Down) && (event.modifiers === Qt.ShiftModifier))) {
            event.accept = true
        }
    }
    Keys.onReleased: {
        if(event.key === Qt.Key_Space) {
            onRequestUserColor()
            event.accept = true
        }
        else if((event.key === Qt.Key_Enter) || (event.key === Qt.Key_Return)) {
            onNewColor()
            event.accept = true
        }
    }
    property int max_text: 100
    onTextChanged: {
        if(parseInt(text) > max_text)
            text = max_text
    }

    font.family: font_extra_light.name
    font.pixelSize: 96
    color: Qt.rgba(color_data.display_text_color[0] / 255,
                   color_data.display_text_color[1] / 255,
                   color_data.display_text_color[2] / 255,
                   1)
    cursorDelegate: Rectangle {
        visible: parent.cursorVisible
        width: parent.cursorRectangle.width / 2

        color: parent.color
        SequentialAnimation on opacity {
            running: true
            loops: Animation.Infinite

            NumberAnimation {
                from: 0.7
                to: 0
                duration: 1000
                easing.type: "InSine"
            }
            NumberAnimation {
                to: 0
                duration: 2000
            }
        }
    }
}
