// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#ifndef COLOR_H
#define COLOR_H

#include <QObject>
#include <QString>
#include <QStringList>

extern "C" {
#define CMS_NO_REGISTER_KEYWORD 1

#include "lcms2.h"
}

// ColorBase -----------------------------------------------------------------

class ColorBase: public QObject {
    Q_OBJECT
public:
    explicit ColorBase(QObject* parent = nullptr);

    enum ICCProfileType: int {
        first,
        RGB,
        HSV,
        HSL,
        Lab,
        LCh,
        last
    };
    Q_ENUM(ICCProfileType)
public:
    // Please notice that this path may be empty when the type is Lab or LCh
    typedef QString ICCProfilePath;
};

// ColorConverter ------------------------------------------------------------

class ColorConverter: public ColorBase {
    Q_OBJECT
public:
    explicit ColorConverter(ColorBase* parent = nullptr);

    void convert(const QStringList& primary_color,
                 QStringList& secondary_color,
                 QStringList& tertiary_color,
                 QStringList& display_color,
                 QStringList& display_text_color);
    void generate(QStringList& primary_color,
                  QStringList& secondary_color,
                  QStringList& tertiary_color,
                  QStringList& display_color,
                  QStringList& display_text_color);
    void convertFromsRGB(const QStringList& sRGB_color,
                         QStringList& primary_color);
private:
    void convertFromPrimary(const QStringList& primary_color);
    void convertTo(QStringList& color,
                   bool is_modified,
                   void (& reset_modified)(),
                   ICCProfileType type,
                   ICCProfilePath path,
                   cmsUInt32Number fType,
                   cmsHPROFILE& hProfile,
                   cmsHTRANSFORM& hTransform);
    static void resetPrimaryModified();
    static void resetSecondaryModified();
    static void resetTertiaryModified();
    static void resetDisplayModified();

    void convertTo1(double cColor[3], QStringList& color, ICCProfileType type);

    void convertToDisplayTextColor(QStringList& display_text_color);

    cmsUInt32Number fPrimary;
    cmsHPROFILE hPrimary;
    cmsFloat64Number cPrimary[3];

    cmsUInt32Number fsRGB;
    cmsHPROFILE hsRGB;
    cmsHTRANSFORM hFromsRGB;

    cmsUInt32Number fSecondary;
    cmsUInt32Number fTertiary;
    cmsUInt32Number fDisplay;
    cmsUInt32Number fDisplayText = TYPE_RGB_8;
    cmsHPROFILE hSecondary;
    cmsHPROFILE hTertiary;
    cmsHPROFILE hDisplay;
    cmsHPROFILE hDisplayText;
    cmsHTRANSFORM hToSecondary;
    cmsHTRANSFORM hToTertiary;
    cmsHTRANSFORM hToDisplay;
    cmsHTRANSFORM hToDisplayText;

    cmsFloat64Number cBuffer[3];

    const double color_epsilon = 1 / 512;
};

extern ColorConverter color_converter;

#endif // COLOR_H
