// ---------------------------------------------------------------------------
//
//  Color Reciter
//  Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#include <cstdio>

#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QFile>

#include "filereadstream.h"
#include "filewritestream.h"
#include "encodedstream.h"
#include "prettywriter.h"
#include "pointer.h"
using namespace rapidjson;

#include "preference.h"

// The preference
Preference preference;

// The ICC Profile Finder
ColorICCProfileFinder icc_profile_finder;

// Preference ----------------------------------------------------------------

Preference::Preference (ColorBase* parent): ColorBase(parent) {}

void Preference::loadPreference() {
    if(buffer == nullptr)
        buffer = new char[1024];

    checkBuiltinICCProfiles();

    if(!preference_directory.exists())
        preference_directory.mkdir(".");
    if(preference_file.exists()) {

        // http://rapidjson.org/md_doc_stream.html#AutoUTFInputStream
#if(defined(Q_OS_UNIX) || defined(Q_OS_MAC))
        FILE* fp = fopen(preference_file.absoluteFilePath().toStdString().c_str(), "r");
#elif defined(Q_OS_WIN)
        FILE* fp = fopen(preference_file.absoluteFilePath().toStdString().c_str(), "rb");
#endif

        FileReadStream bis(fp, buffer, 1024);
        AutoUTFInputStream<unsigned, FileReadStream> eis(bis);
        preference.ParseStream<0, AutoUTF<unsigned> >(eis);

        fclose(fp);
    }

    validatePreference();
    savePreference();

    is_loaded = true;
}

void Preference::checkBuiltinICCProfiles() {
    if(!builtin_icc_profiles_directory.exists())
        builtin_icc_profiles_directory.mkpath(".");

    QDirIterator it(":/icc profiles", QDirIterator::NoIteratorFlags);
    while(it.hasNext()) {
        it.next();

        if(!QFileInfo(builtin_icc_profiles_directory, it.fileName()).exists())
            QFile::copy(it.filePath(),
                        builtin_icc_profiles_directory.absoluteFilePath(it.fileName()));
    }
}

void Preference::validatePreference() {
    if(buffer == nullptr)
        buffer = new char[1024];

    if(!preference.IsObject()) {
        preference.SetObject();

        is_modified_within_loading = true;
    }

    // primary_icc_profile
    if(!preference.HasMember("primary_icc_profile_type")) {
        preference.AddMember("primary_icc_profile_type",
                             Value().SetInt(ICCProfileType::HSV),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(!preference["primary_icc_profile_type"].IsNumber() ||
            !(preference["primary_icc_profile_type"].GetInt() > ICCProfileType::first &&
              preference["primary_icc_profile_type"].GetInt() < ICCProfileType::last)) {
        preference["primary_icc_profile_type"].SetInt(ICCProfileType::HSV);

        is_modified_within_loading = true;
    }
    else
        ;

    if(!preference.HasMember("primary_icc_profile_path")) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference.AddMember("primary_icc_profile_path",
                             Value().SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator()),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(preference["primary_icc_profile_path"].IsString() &&
            QString(preference["primary_icc_profile_path"].GetString()).isEmpty() &&
            (preference["primary_icc_profile_type"].GetInt() == ICCProfileType::Lab ||
             preference["primary_icc_profile_type"].GetInt() == ICCProfileType::LCh))
        ;
    else if(!preference["primary_icc_profile_path"].IsString() ||
            !QFileInfo(preference["primary_icc_profile_path"].GetString()).exists()) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference["primary_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else
        ;

    // secondary_icc_profile
    if(!preference.HasMember("secondary_icc_profile_type")) {
        preference.AddMember("secondary_icc_profile_type",
                             Value().SetInt(ICCProfileType::RGB),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(!preference["secondary_icc_profile_type"].IsNumber() ||
            !(preference["secondary_icc_profile_type"].GetInt() > ICCProfileType::first &&
              preference["secondary_icc_profile_type"].GetInt() < ICCProfileType::last)) {
        preference["secondary_icc_profile_type"].SetInt(ICCProfileType::RGB);

        is_modified_within_loading = true;
    }
    else
        ;

    if(!preference.HasMember("secondary_icc_profile_path")) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference.AddMember("secondary_icc_profile_path",
                             Value().SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator()),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(preference["secondary_icc_profile_path"].IsString() &&
            QString(preference["secondary_icc_profile_path"].GetString()).isEmpty() &&
            (preference["secondary_icc_profile_type"].GetInt() == ICCProfileType::Lab ||
             preference["secondary_icc_profile_type"].GetInt() == ICCProfileType::LCh))
        ;
    else if(!preference["secondary_icc_profile_path"].IsString() ||
            !QFileInfo(preference["secondary_icc_profile_path"].GetString()).exists()) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference["secondary_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else
        ;

    // tertiary_icc_profile
    if(!preference.HasMember("tertiary_icc_profile_type")) {
        preference.AddMember("tertiary_icc_profile_type",
                             Value().SetInt(ICCProfileType::Lab),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(!preference["tertiary_icc_profile_type"].IsNumber() ||
            !(preference["tertiary_icc_profile_type"].GetInt() > ICCProfileType::first &&
              preference["tertiary_icc_profile_type"].GetInt() < ICCProfileType::last)) {
        preference["tertiary_icc_profile_type"].SetInt(ICCProfileType::Lab);

        is_modified_within_loading = true;
    }
    else
        ;

    if(!preference.HasMember("tertiary_icc_profile_path")) {
        preference.AddMember("tertiary_icc_profile_path",
                             Value().SetString(""),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(preference["tertiary_icc_profile_path"].IsString() &&
            QString(preference["tertiary_icc_profile_path"].GetString()).isEmpty() &&
            (preference["tertiary_icc_profile_type"].GetInt() == ICCProfileType::Lab ||
             preference["tertiary_icc_profile_type"].GetInt() == ICCProfileType::LCh))
        ;
    else if(!preference["tertiary_icc_profile_path"].IsString() ||
            !QFileInfo(preference["tertiary_icc_profile_path"].GetString()).exists()) {
        preference["tertiary_icc_profile_path"].SetString("");

        is_modified_within_loading = true;
    }
    else
        ;

    // display_icc_profile
    if(!preference.HasMember("display_icc_profile_path")) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference.AddMember("display_icc_profile_path",
                             Value().SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator()),
                             preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else if(!preference["display_icc_profile_path"].IsString() ||
            !QFileInfo(preference["display_icc_profile_path"].GetString()).exists()) {
        memset(buffer, 0, 1024);
        strcpy(buffer, builtin_icc_profiles_directory.absoluteFilePath("sRGB v4.icc").toStdString().c_str());

        preference["display_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(strlen(buffer)), preference.GetAllocator());

        is_modified_within_loading = true;
    }
    else
        ;

    primary_icc_profile_modified = true;
    secondary_icc_profile_modified = true;
    tertiary_icc_profile_modified = true;
    display_icc_profile_modified = true;
}
bool Preference::isModifiedWithinLoading() {
    return is_modified_within_loading;
}
void Preference::savePreference() {
    if(buffer == nullptr)
        buffer = new char[1024];

    if(!preference_directory.exists())
        preference_directory.mkpath(".");

    // http://rapidjson.org/md_doc_stream.html#AutoUTFOutputStream
#if(defined(Q_OS_UNIX) || defined(Q_OS_MAC))
    FILE* fp = fopen(preference_file.absoluteFilePath().toStdString().c_str(), "w");
#elif defined(Q_OS_WIN)
    FILE* fp = fopen(preference_file.absoluteFilePath().toStdString().c_str(), "wb");
#endif

    FileWriteStream os(fp, buffer, 1024);
    PrettyWriter<FileWriteStream> writer(os);
    preference.Accept(writer);

    fclose(fp);
}

void Preference::freeBuffer() {
    if(buffer != nullptr) {
        delete [] buffer;
        buffer = nullptr;
    }
}

Preference::ICCProfileType Preference::getPrimaryICCProfileType() {
    if(!is_loaded) loadPreference();

    return static_cast<ICCProfileType>(preference["primary_icc_profile_type"].GetInt());
}
Preference::ICCProfileType Preference::getSecondaryICCProfileType() {
    if(!is_loaded) loadPreference();

    return static_cast<ICCProfileType>(preference["secondary_icc_profile_type"].GetInt());
}
Preference::ICCProfileType Preference::getTertiaryICCProfileType() {
    if(!is_loaded) loadPreference();

    return static_cast<ICCProfileType>(preference["tertiary_icc_profile_type"].GetInt());
}
Preference::ICCProfilePath Preference::getPrimaryICCProfilePath() {
    if(!is_loaded) loadPreference();

    return preference["primary_icc_profile_path"].GetString();
}
Preference::ICCProfilePath Preference::getSecondaryICCProfilePath() {
    if(!is_loaded) loadPreference();

    return preference["secondary_icc_profile_path"].GetString();
}
Preference::ICCProfilePath Preference::getTertiaryICCProfilePath() {
    if(!is_loaded) loadPreference();

    return preference["tertiary_icc_profile_path"].GetString();
}
Preference::ICCProfilePath Preference::getDisplayICCProfilePath() {
    if(!is_loaded) loadPreference();

    return preference["display_icc_profile_path"].GetString();
}

void Preference::setPrimaryICCProfileType(const ICCProfileType type) {
    preference["primary_icc_profile_type"].SetInt(type);
    savePreference();
    primary_icc_profile_modified = true;
}
void Preference::setSecondaryICCProfileType(const ICCProfileType type) {
    preference["secondary_icc_profile_type"].SetInt(type);
    savePreference();
    secondary_icc_profile_modified = true;
}
void Preference::setTertiaryICCProfileType(const ICCProfileType type) {
    preference["tertiary_icc_profile_type"].SetInt(type);
    savePreference();
    tertiary_icc_profile_modified = true;
}
void Preference::setPrimaryICCProfilePath(const ICCProfilePath& path) {
    if(buffer == nullptr)
        buffer = new char[1024];

    int len = sprintf(buffer, "%s", path.toStdString().c_str());
    preference["primary_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(len), preference.GetAllocator());
    memset(buffer, 0, 1024);

    savePreference();

    primary_icc_profile_modified = true;
}
void Preference::setSecondaryICCProfilePath(const ICCProfilePath& path) {
    if(buffer == nullptr)
        buffer = new char[1024];

    int len = sprintf(buffer, "%s", path.toStdString().c_str());
    preference["secondary_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(len), preference.GetAllocator());
    memset(buffer, 0, 1024);

    savePreference();

    secondary_icc_profile_modified = true;
}
void Preference::setTertiaryICCProfilePath(const ICCProfilePath& path) {
    if(buffer == nullptr)
        buffer = new char[1024];

    int len = sprintf(buffer, "%s", path.toStdString().c_str());
    preference["tertiary_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(len), preference.GetAllocator());
    memset(buffer, 0, 1024);

    savePreference();

    tertiary_icc_profile_modified = true;
}
void Preference::setDisplayICCProfilePath(const ICCProfilePath& path) {
    if(buffer == nullptr)
        buffer = new char[1024];

    int len = sprintf(buffer, "%s", path.toStdString().c_str());
    preference["display_icc_profile_path"].SetString(buffer, static_cast<unsigned int>(len), preference.GetAllocator());
    memset(buffer, 0, 1024);

    savePreference();

    display_icc_profile_modified = true;
}

bool Preference::isPrimaryICCProfileModified() const {
    return primary_icc_profile_modified;
}
bool Preference::isSecondaryICCProfileModified() const {
    return secondary_icc_profile_modified;
}
bool Preference::isTertiaryICCProfileModified() const {
    return tertiary_icc_profile_modified;
}
bool Preference::isDisplayICCProfileModified() const {
    return display_icc_profile_modified;
}
void Preference::resetPrimaryICCProfileModified() {
    primary_icc_profile_modified = false;
}
void Preference::resetSecondaryICCProfileModified() {
    secondary_icc_profile_modified = false;
}
void Preference::resetTertiaryICCProfileModified() {
    tertiary_icc_profile_modified = false;
}
void Preference::resetDisplayICCProfileModified() {
    display_icc_profile_modified = false;
}

// ColorICCProfileFinder -----------------------------------------------------

ColorICCProfileFinder::ColorICCProfileFinder(ColorBase* parent): ColorBase(parent) {}

void ColorICCProfileFinder::loadNameList() {
    // get builtin icc profile location
    QStringList builtin_locations;
    builtin_locations.append(preference.builtin_icc_profiles_directory.absolutePath());

    // get system icc profile location
    QStringList system_locations;
#if defined(Q_OS_LINUX)
    system_locations.append("/usr/share/color/icc");
    system_locations.append("/usr/local/share/color/icc");
    system_locations.append(QDir::homePath() + QDir::separator() + ".color/icc");
#elif defined(Q_OS_MAC)
    system_locations.append("/Library/ColorSync/Profiles");
    system_locations.append("/Library/Application Support/Adobe/Color/Profiles");
    system_locations.append(QDir::homePath() + QDir::separator() + "Library/ColorSync/Profiles");
#elif defined(Q_OS_WIN)
    system_locations.append("C:/WINDOWS/System32/spool/drivers/color");
#endif

    // get the filter
    QStringList filter; filter.append(QString("*.icc"));

    // load the profiles
    name_map.insert("", "");

    loadNameList1(builtin_locations, filter, "Built-in");
    loadNameList1(system_locations, filter);
}
void ColorICCProfileFinder::loadNameList1(QStringList& locations, QStringList& filter, QString comment) {
    for(auto location = locations.begin(); location != locations.end(); ++location) {
        QDirIterator it(*location, filter, QDir::NoFilter, QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);

        QFileInfo file;
        while(it.hasNext()) {
            it.next();

            file = it.fileInfo();
            if(comment.isEmpty())
                name_map.insert(file.completeBaseName(), file.absoluteFilePath());
            else name_map.insert(file.completeBaseName() + " (" + comment + ")", file.absoluteFilePath());
        }
    }
}

void ColorICCProfileFinder::getNameList(QStringList& name_list) {
    if(name_map.isEmpty())
        loadNameList();

    for(auto it = name_map.begin(); it != name_map.end(); ++it)
        name_list.append(it.key());
}
QString ColorICCProfileFinder::Name2Path(const QString& name) {
    if(name_map.isEmpty())
        loadNameList();

    if(name.isEmpty()) return name;
    else return name_map[name];
}
QString ColorICCProfileFinder::Path2Name(const QString& path) {
    if(name_map.isEmpty())
        loadNameList();

    if(path.isEmpty()) return path;
    else if(name_map.contains(QFileInfo(path).completeBaseName() + " (Built-in)")) // because built-in icc profiles are at the front of the map
        return QFileInfo(path).completeBaseName() + " (Built-in)";
    else return QFileInfo(path).completeBaseName();
}
