function Component() {
    // default constructor
}

Component.prototype.createOperations = function() {
    component.createOperations();

    if(systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/color-reciter.exe", "@StartMenuDir@/Color Reciter.lnk",
            "workingDirectory=@TargetDir@", "description=Color Reciter");
        component.addOperation("CreateShortcut", "@TargetDir@/MaintenanceTool.exe", "@StartMenuDir@/Color Reciter Maintenance Tool.lnk",
            "workingDirectory=@TargetDir@", "description=Color Reciter");
    }
}
